package com.example.gitgradle.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GreetingServiceTest {
	
	private GreetingService svc;
	
	@BeforeEach
	public void setUp() {
		svc = new GreetingService();		
	}
	
	@Test
	public void canGreetInEnglish() {
		String expected = "Hello";
		String actual = svc.greet("EN");
		assertEquals(expected, actual);
	}
}
